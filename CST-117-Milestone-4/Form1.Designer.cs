﻿namespace CST_117_Milestone_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.addBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.title = new System.Windows.Forms.Label();
            this.checkBox = new System.Windows.Forms.Button();
            this.xBox = new System.Windows.Forms.Button();
            this.fullBox = new System.Windows.Forms.TextBox();
            this.stockBox = new System.Windows.Forms.TextBox();
            this.pBox = new System.Windows.Forms.TextBox();
            this.priceBox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.expBox = new System.Windows.Forms.TextBox();
            this.descBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.infoButton = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.addToolTip = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.addToolTip);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.addBox);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.searchBox);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.addButton);
            this.groupBox2.Controls.Add(this.searchButton);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(1083, 20);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.groupBox2.Size = new System.Drawing.Size(649, 364);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "FIND/ADD";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(201, 49);
            this.button1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 56);
            this.button1.TabIndex = 27;
            this.button1.Text = "?";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // addBox
            // 
            this.addBox.BackColor = System.Drawing.SystemColors.Control;
            this.addBox.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addBox.Location = new System.Drawing.Point(14, 261);
            this.addBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.addBox.Name = "addBox";
            this.addBox.Size = new System.Drawing.Size(468, 62);
            this.addBox.TabIndex = 22;
            this.addBox.TextChanged += new System.EventHandler(this.AddBox_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(2, 199);
            this.label8.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(228, 55);
            this.label8.TabIndex = 23;
            this.label8.Text = "Add Item:";
            // 
            // searchBox
            // 
            this.searchBox.BackColor = System.Drawing.SystemColors.Control;
            this.searchBox.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBox.Location = new System.Drawing.Point(14, 114);
            this.searchBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(468, 62);
            this.searchBox.TabIndex = 16;
            this.searchBox.TextChanged += new System.EventHandler(this.SearchBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(2, 51);
            this.label7.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(190, 55);
            this.label7.TabIndex = 12;
            this.label7.Text = "Search:";
            // 
            // addButton
            // 
            this.addButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addButton.BackColor = System.Drawing.SystemColors.Control;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addButton.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.Location = new System.Drawing.Point(506, 243);
            this.addButton.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(117, 112);
            this.addButton.TabIndex = 17;
            this.addButton.Text = "+";
            this.addButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // searchButton
            // 
            this.searchButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.searchButton.BackColor = System.Drawing.SystemColors.Control;
            this.searchButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.searchButton.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchButton.Location = new System.Drawing.Point(506, 96);
            this.searchButton.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(117, 112);
            this.searchButton.TabIndex = 21;
            this.searchButton.Text = "🔍";
            this.searchButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.searchButton.UseVisualStyleBackColor = false;
            this.searchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.BackColor = System.Drawing.Color.Transparent;
            this.title.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(252, 20);
            this.title.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(525, 94);
            this.title.TabIndex = 27;
            this.title.Text = "INVENTORY";
            // 
            // checkBox
            // 
            this.checkBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBox.BackColor = System.Drawing.Color.White;
            this.checkBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox.Location = new System.Drawing.Point(376, 1249);
            this.checkBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(117, 112);
            this.checkBox.TabIndex = 25;
            this.checkBox.Text = "✓";
            this.checkBox.UseVisualStyleBackColor = false;
            this.checkBox.Visible = false;
            this.checkBox.Click += new System.EventHandler(this.CheckBox_Click);
            // 
            // xBox
            // 
            this.xBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.xBox.BackColor = System.Drawing.Color.White;
            this.xBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.xBox.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xBox.Location = new System.Drawing.Point(506, 1249);
            this.xBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.xBox.Name = "xBox";
            this.xBox.Size = new System.Drawing.Size(117, 112);
            this.xBox.TabIndex = 24;
            this.xBox.Text = "✗";
            this.xBox.UseVisualStyleBackColor = false;
            this.xBox.Visible = false;
            this.xBox.Click += new System.EventHandler(this.XBox_Click);
            // 
            // fullBox
            // 
            this.fullBox.Location = new System.Drawing.Point(264, 283);
            this.fullBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.fullBox.Name = "fullBox";
            this.fullBox.ReadOnly = true;
            this.fullBox.Size = new System.Drawing.Size(340, 62);
            this.fullBox.TabIndex = 11;
            // 
            // stockBox
            // 
            this.stockBox.Location = new System.Drawing.Point(168, 172);
            this.stockBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.stockBox.Name = "stockBox";
            this.stockBox.ReadOnly = true;
            this.stockBox.Size = new System.Drawing.Size(436, 62);
            this.stockBox.TabIndex = 10;
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(250, 60);
            this.pBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.pBox.Name = "pBox";
            this.pBox.ReadOnly = true;
            this.pBox.Size = new System.Drawing.Size(354, 62);
            this.pBox.TabIndex = 9;
            // 
            // priceBox
            // 
            this.priceBox.Location = new System.Drawing.Point(201, 395);
            this.priceBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.priceBox.Name = "priceBox";
            this.priceBox.ReadOnly = true;
            this.priceBox.Size = new System.Drawing.Size(403, 62);
            this.priceBox.TabIndex = 8;
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(1353, 397);
            this.button3.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(117, 112);
            this.button3.TabIndex = 31;
            this.button3.Text = "✎";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(1484, 397);
            this.button2.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 112);
            this.button2.TabIndex = 30;
            this.button2.Text = "↺";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // expBox
            // 
            this.expBox.Location = new System.Drawing.Point(264, 506);
            this.expBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.expBox.Name = "expBox";
            this.expBox.ReadOnly = true;
            this.expBox.Size = new System.Drawing.Size(340, 62);
            this.expBox.TabIndex = 7;
            // 
            // descBox
            // 
            this.descBox.Location = new System.Drawing.Point(23, 685);
            this.descBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.descBox.Multiline = true;
            this.descBox.Name = "descBox";
            this.descBox.ReadOnly = true;
            this.descBox.Size = new System.Drawing.Size(594, 546);
            this.descBox.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 625);
            this.label6.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(277, 55);
            this.label6.TabIndex = 5;
            this.label6.Text = "Description:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 513);
            this.label5.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(248, 55);
            this.label5.TabIndex = 4;
            this.label5.Text = "Exp. Date:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 402);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 55);
            this.label4.TabIndex = 3;
            this.label4.Text = "Price: $";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 290);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(246, 55);
            this.label3.TabIndex = 2;
            this.label3.Text = "Full Stock:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 178);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 55);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stock:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 67);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "Product #:";
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button4.BackColor = System.Drawing.SystemColors.Control;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(1615, 397);
            this.button4.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(117, 112);
            this.button4.TabIndex = 32;
            this.button4.Text = "🗑";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.infoButton);
            this.groupBox1.Controls.Add(this.checkBox);
            this.groupBox1.Controls.Add(this.xBox);
            this.groupBox1.Controls.Add(this.fullBox);
            this.groupBox1.Controls.Add(this.stockBox);
            this.groupBox1.Controls.Add(this.pBox);
            this.groupBox1.Controls.Add(this.priceBox);
            this.groupBox1.Controls.Add(this.expBox);
            this.groupBox1.Controls.Add(this.descBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(1083, 522);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.groupBox1.Size = new System.Drawing.Size(649, 1374);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ITEM INFO";
            // 
            // infoButton
            // 
            this.infoButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.infoButton.BackColor = System.Drawing.Color.White;
            this.infoButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.infoButton.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoButton.Location = new System.Drawing.Point(23, 1249);
            this.infoButton.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.infoButton.Name = "infoButton";
            this.infoButton.Size = new System.Drawing.Size(117, 112);
            this.infoButton.TabIndex = 26;
            this.infoButton.Text = "?";
            this.infoButton.UseVisualStyleBackColor = false;
            this.infoButton.Click += new System.EventHandler(this.InfoButton_Click);
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.SystemColors.Control;
            this.listBox1.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 73;
            this.listBox1.Items.AddRange(new object[] {
            "Carrots",
            "Cauliflower",
            "Broccoli",
            "Asparagus",
            "Iceberg",
            "Romaine",
            "Mixed",
            "Apple",
            "Orange",
            "Lemon",
            "Avocado",
            "Mandarin",
            "Orange",
            "Lime",
            "Grapefruit",
            "Watermelon",
            "Blueberry",
            "Rasberry",
            "Strawberry"});
            this.listBox1.Location = new System.Drawing.Point(33, 120);
            this.listBox1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(1026, 1756);
            this.listBox1.TabIndex = 28;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.ListBox1_SelectedIndexChanged);
            // 
            // addToolTip
            // 
            this.addToolTip.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addToolTip.BackColor = System.Drawing.Color.White;
            this.addToolTip.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addToolTip.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addToolTip.Location = new System.Drawing.Point(233, 199);
            this.addToolTip.Margin = new System.Windows.Forms.Padding(7);
            this.addToolTip.Name = "addToolTip";
            this.addToolTip.Size = new System.Drawing.Size(58, 56);
            this.addToolTip.TabIndex = 28;
            this.addToolTip.Text = "?";
            this.addToolTip.UseVisualStyleBackColor = false;
            this.addToolTip.Click += new System.EventHandler(this.AddToolTip_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1764, 1916);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.title);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.Name = "Form1";
            this.Text = "Inventory";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox addBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Button checkBox;
        private System.Windows.Forms.Button xBox;
        private System.Windows.Forms.TextBox fullBox;
        private System.Windows.Forms.TextBox stockBox;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.TextBox priceBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox expBox;
        private System.Windows.Forms.TextBox descBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button infoButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button addToolTip;
    }
}


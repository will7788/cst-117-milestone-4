﻿namespace CST_117_Milestone_4
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBox = new System.Windows.Forms.Button();
            this.xBox = new System.Windows.Forms.Button();
            this.fullBox = new System.Windows.Forms.TextBox();
            this.stockBox = new System.Windows.Forms.TextBox();
            this.pBox = new System.Windows.Forms.TextBox();
            this.priceBox = new System.Windows.Forms.TextBox();
            this.expBox = new System.Windows.Forms.TextBox();
            this.descBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.nameBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.checkBox);
            this.groupBox1.Controls.Add(this.xBox);
            this.groupBox1.Controls.Add(this.fullBox);
            this.groupBox1.Controls.Add(this.stockBox);
            this.groupBox1.Controls.Add(this.pBox);
            this.groupBox1.Controls.Add(this.priceBox);
            this.groupBox1.Controls.Add(this.expBox);
            this.groupBox1.Controls.Add(this.descBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(28, 27);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(7);
            this.groupBox1.Size = new System.Drawing.Size(1073, 1486);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ITEM INFO";
            this.groupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(196, 62);
            this.nameBox.Margin = new System.Windows.Forms.Padding(7);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(858, 62);
            this.nameBox.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 69);
            this.label7.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(166, 55);
            this.label7.TabIndex = 26;
            this.label7.Text = "Name:";
            // 
            // checkBox
            // 
            this.checkBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBox.BackColor = System.Drawing.Color.White;
            this.checkBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBox.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox.Location = new System.Drawing.Point(812, 1345);
            this.checkBox.Margin = new System.Windows.Forms.Padding(7);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(117, 112);
            this.checkBox.TabIndex = 25;
            this.checkBox.Text = "✓";
            this.checkBox.UseVisualStyleBackColor = false;
            this.checkBox.Click += new System.EventHandler(this.CheckBox_Click);
            // 
            // xBox
            // 
            this.xBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.xBox.BackColor = System.Drawing.Color.White;
            this.xBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.xBox.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xBox.Location = new System.Drawing.Point(943, 1345);
            this.xBox.Margin = new System.Windows.Forms.Padding(7);
            this.xBox.Name = "xBox";
            this.xBox.Size = new System.Drawing.Size(117, 112);
            this.xBox.TabIndex = 24;
            this.xBox.Text = "✗";
            this.xBox.UseVisualStyleBackColor = false;
            this.xBox.Click += new System.EventHandler(this.XBox_Click);
            // 
            // fullBox
            // 
            this.fullBox.Location = new System.Drawing.Point(268, 395);
            this.fullBox.Margin = new System.Windows.Forms.Padding(7);
            this.fullBox.Name = "fullBox";
            this.fullBox.Size = new System.Drawing.Size(786, 62);
            this.fullBox.TabIndex = 11;
            // 
            // stockBox
            // 
            this.stockBox.Location = new System.Drawing.Point(173, 283);
            this.stockBox.Margin = new System.Windows.Forms.Padding(7);
            this.stockBox.Name = "stockBox";
            this.stockBox.Size = new System.Drawing.Size(881, 62);
            this.stockBox.TabIndex = 10;
            this.stockBox.TextChanged += new System.EventHandler(this.StockBox_TextChanged);
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(254, 172);
            this.pBox.Margin = new System.Windows.Forms.Padding(7);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(800, 62);
            this.pBox.TabIndex = 9;
            // 
            // priceBox
            // 
            this.priceBox.Location = new System.Drawing.Point(205, 506);
            this.priceBox.Margin = new System.Windows.Forms.Padding(7);
            this.priceBox.Name = "priceBox";
            this.priceBox.Size = new System.Drawing.Size(849, 62);
            this.priceBox.TabIndex = 8;
            // 
            // expBox
            // 
            this.expBox.Location = new System.Drawing.Point(268, 618);
            this.expBox.Margin = new System.Windows.Forms.Padding(7);
            this.expBox.Name = "expBox";
            this.expBox.Size = new System.Drawing.Size(786, 62);
            this.expBox.TabIndex = 7;
            // 
            // descBox
            // 
            this.descBox.Location = new System.Drawing.Point(23, 781);
            this.descBox.Margin = new System.Windows.Forms.Padding(7);
            this.descBox.Multiline = true;
            this.descBox.Name = "descBox";
            this.descBox.Size = new System.Drawing.Size(1031, 546);
            this.descBox.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 721);
            this.label6.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(277, 55);
            this.label6.TabIndex = 5;
            this.label6.Text = "Description:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 625);
            this.label5.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(248, 55);
            this.label5.TabIndex = 4;
            this.label5.Text = "Exp. Date:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 513);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 55);
            this.label4.TabIndex = 3;
            this.label4.Text = "Price: $";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 402);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(246, 55);
            this.label3.TabIndex = 2;
            this.label3.Text = "Full Stock:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 290);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 55);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stock:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 178);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "Product #:";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1129, 1526);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "Form2";
            this.Text = "Add An Item";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button checkBox;
        private System.Windows.Forms.Button xBox;
        private System.Windows.Forms.TextBox fullBox;
        private System.Windows.Forms.TextBox stockBox;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.TextBox priceBox;
        private System.Windows.Forms.TextBox expBox;
        private System.Windows.Forms.TextBox descBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label label7;
    }
}